# 1. Create table using SQl
CREATE TABLE OLYMPIC(
    ID serial,
    Name VARCHAR(200),
    Sex VARCHAR(5),
    Age INTEGER,
    Height DECIMAL,
    Weight DECIMAL,
    Team VARCHAR(200),
    NOC VARCHAR(200),
    Games VARCHAR(200),
    Year INTEGER,
    Season VARCHAR(100),
    City VARCHAR(300),
    Sport VARCHAR(300),
    Event VARCHAR(300),
    Medal VARCHAR(200)
);

# 2. Retrieve all the values from table
   select * from table;

# 3. To view all the uses/table
   \dt

# 4. Drop the table
   drop table table_name;

# 5. Copy csv value from a file and store in the database
   COPY OLYMPIC(ID,Name,Sex,Age,Height,Weight,Team,NOC,Games,Year,Season,City,Sport,Event,Medal) FROM '/home/kiran/Desktop/athlete_events.csv' DELIMITER ',' CSV HEADER;

# 6. start database
   sudo su - postgres

# 7. start shell
   psql

# - Number of times olympics hosted per name of city over the years - Piechart
select City,count(*) as Total from OLYMPIC group by City;
# https://www.tutorialspoint.com/count-number-of-times-value-appears-in-particular-column-in-mysql


#  Top 10 countries who have won most medals after 2000 - stacked column - split gold/silver/bronze

select Team,count(*) as Total from OLYMPIC group by Team  order by Total desc;

# https://www.tutorialspoint.com/sql/sql-sorting-results.htm

# - Find out all medal winners from India
SELECT Year, Name FROM OLYMPIC WHERE Team = 'India' and Medal IS NOT NULL;


# M/F participation by decade - column chart


# Per season average age of athletes who participated in Boxing Men’s Heavyweight - Line Chart


