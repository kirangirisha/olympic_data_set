import unittest
import medal_by_india


class TestHostCity(unittest.TestCase):
    
    def test_india_medal(self):
        actual_result = medal_by_india.indian_medal("./india_specific_athlete_events.csv")

        expected_result = {'1928': ['Shaukat Ali', 'Richard James Allen'], 
                          '1964': ['Syed Mushtaq Ali'], '1932': ['Richard James Allen'], 
                          '1936': ['Richard James Allen'], '2008': ['Abhinav Bindra'], 
                          '1980': ['Bir Bahadur Chettri']
                          }

        self.assertDictEqual(actual_result, expected_result)
   

if __name__ == '__main__':
    unittest.main()

