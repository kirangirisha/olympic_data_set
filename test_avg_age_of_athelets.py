import unittest
import avg_age_of_athelets


class TestHostCity(unittest.TestCase):
    
    def test_hosting_city(self):
        actual_result = avg_age_of_athelets.avg_age_wreslers("./athlete_events.csv")

        expected_result = {'1992': 24.32, '2012': 25.96, '1920': 29.28, '1900': 29.01, 
                          '1988': 24.08, '1994': 24.41, '1932': 32.58, '2002': 25.91, 
                          '1952': 26.16, '1980': 23.69, '2000': 25.42, '1996': 24.91, 
                          '1912': 27.53, '1924': 28.37, '2014': 25.98, '1948': 28.78, 
                          '1998': 25.16, '2006': 25.95, '2008': 25.73, '2016': 26.21, 
                          '2004': 25.64, '1960': 25.17, '1964': 24.94, '1984': 23.9, 
                          '1968': 24.24, '1972': 24.3, '1936': 27.53, '1956': 25.92, 
                          '1928': 29.11, '1976': 23.84, '2010': 26.12, '1906': 27.1, 
                          '1904': 26.68, '1908': 26.96, '1896': 23.48
                          }

        self.assertDictEqual(actual_result, expected_result)
   

if __name__ == '__main__':
    unittest.main()

