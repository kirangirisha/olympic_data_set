import unittest
import male_female_ration


class TestHostCity(unittest.TestCase):
    
    def test_hosting_city(self):
        actual_result = male_female_ration.decade_participation("./athlete_events.csv")

        expected_result = {'1895 - 1905': {'M': 3568, 'F': 49}, '1905 - 1915': {'M': 7007, 'F': 134}, 
                           '1915 - 1925': {'M': 5432, 'F': 261}, '1925 - 1935': {'M': 2952, 'F': 369}, 
                           '1935 - 1945': {}, '1945 - 1955': {'M': 7676, 'F': 1682}, 
                           '1955 - 1965': {'M': 15233, 'F': 3482}, '1965 - 1975': {'M': 9351, 'F': 2608},
                            '1975 - 1985': {'M': 15356, 'F': 5169}, '1985 - 1995': {'M': 13290, 'F': 6283},
                            '1995 - 2005': {'M': 21035, 'F': 13943}, '2005 - 2015': {'M': 20314, 'F': 15501}
                            }

        self.assertDictEqual(actual_result, expected_result)
   

if __name__ == '__main__':
    unittest.main()
