import unittest
import hosting_city


class TestHostCity(unittest.TestCase):
    
    def test_hosting_city(self):
        actual_result = hosting_city.host_city("./athlete_events.csv")

        expected_result = {'Barcelona': 1, 'London': 3, 'Antwerpen': 1, 'Paris': 2, 
                    'Calgary': 1, 'Albertville': 1, 'Lillehammer': 1, 'Los Angeles': 2, 'Salt Lake City': 1, 
                    'Helsinki': 1, 'Lake Placid': 2, 'Sydney': 1, 'Atlanta': 1, 'Stockholm': 2, 'Sochi': 1, 
                    'Nagano': 1, 'Torino': 1, 'Beijing': 1, 'Rio de Janeiro': 1, 'Athina': 3, 'Squaw Valley': 1, 
                    'Innsbruck': 2, 'Sarajevo': 1, 'Mexico City': 1, 'Munich': 1, 'Seoul': 1, 'Berlin': 1, 
                    'Oslo': 1, "Cortina d'Ampezzo": 1, 'Melbourne': 1, 'Roma': 1, 'Amsterdam': 1, 'Montreal': 1, 
                    'Moskva': 1, 'Tokyo': 1, 'Vancouver': 1, 'Grenoble': 1, 'Sapporo': 1, 'Chamonix': 1, 
                    'St. Louis': 1, 'Sankt Moritz': 2, 'Garmisch-Partenkirchen': 1
        }

        self.assertDictEqual(actual_result, expected_result)
   

if __name__ == '__main__':
    unittest.main()

