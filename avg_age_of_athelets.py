import csv
import matplotlib.pyplot as plt

def avg_age_wreslers(csv_path):
    with open(csv_path, 'r') as csv_file:
        matches_reader = csv.DictReader(csv_file)

        avg_age_dict = {}

        for each in matches_reader:
            if each['Year'] not in avg_age_dict:
                avg_age_dict[each['Year']] = []
            else:
                avg_age_dict[each['Year']].append(each['Age'])      
                
        for each in avg_age_dict:
            avg_age = 1
            counter = 1
            for itr in avg_age_dict[each]:
                if itr != 'NA':
                    avg_age = avg_age + int(itr)
                    counter += 1
                else:
                    continue    
            avg_age = avg_age / counter
            avg_age = round(avg_age, 2)
            avg_age_dict[each] = avg_age                  
            
        return avg_age_dict


def plot_graph(avg_age_of_athletes):

    x = list(avg_age_of_athletes.keys())
    y = list(avg_age_of_athletes.values())
    plt.plot(x, y)
    plt.show()


avg_age_of_athletes = avg_age_wreslers("./athlete_events.csv")
plot_graph(avg_age_of_athletes)




















