# - Number of times olympics hosted per name of city over the years - Piechart
import csv
import matplotlib.pyplot as plt

def host_city(csv_path):
    with open(csv_path, 'r') as csv_file:
        matches_reader = csv.DictReader(csv_file)


        host_city_dictionary = {}
        for row in matches_reader:
            if row['City'] not in host_city_dictionary:
                host_city_dictionary[row['City']] = [row['Year']]
            else:
                if row['Year'] not in host_city_dictionary[row['City']]:
                    host_city_dictionary[row['City']].append(row['Year'])
                else:
                    continue    

        for each in host_city_dictionary:
            host_city_dictionary[each] = len(host_city_dictionary[each])
        
        return host_city_dictionary
    

def plot_graph(city_hosted_no_of_times):
    plt.figure(figsize=(5,5))
    labels = list(city_hosted_no_of_times.keys())
    values = list(city_hosted_no_of_times.values())
    plt.pie(values, labels=labels)
    plt.show()
    
city_hosted_no_of_times = host_city('./athlete_events.csv')
plot_graph(city_hosted_no_of_times)