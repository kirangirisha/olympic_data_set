# - M/F participation by decade - column chart

import csv
import operator
from collections import Counter 
import numpy as np
import matplotlib.pyplot as plt

def decade_participation(csv_path):
    
    with open(csv_path, 'r') as csv_file:
        matches_reader = csv.DictReader(csv_file)
        
        decade_data_dictionary = {}
        for row in matches_reader:
            if row["Year"] not in decade_data_dictionary:            
                decade_data_dictionary[row["Year"]] = {row["Sex"] : 1}
                
            else:
                if row["Sex"] not in decade_data_dictionary[row["Year"]]:
                    decade_data_dictionary[row["Year"]][row["Sex"]] = 1
                else:
                    decade_data_dictionary[row["Year"]][row["Sex"]] += 1
        
        decade_participation_dictionary = {}
        start = 1895
        end = start + 10 # 1904
        temp_dict_var = {}
        for each_year in sorted(decade_data_dictionary):
            if int(each_year) >= start and int(each_year) <= end:
    	#1896': #each_year
		#'1906': {'M': 1722, 'F': 11}, '1904': {'M': 1285, 'F': 16}, '1908': {'M': 3054, 'F': 47},
                if 'M' in decade_data_dictionary[each_year]: # {'M': 1285, 'F': 16}
                    if 'M' in temp_dict_var:
                        temp_dict_var["M"] += decade_data_dictionary[each_year]['M']
                    else:
                        temp_dict_var["M"] = decade_data_dictionary[each_year]['M']
                
                if 'F' in decade_data_dictionary[each_year]: 
                    if 'F' in temp_dict_var:
                        temp_dict_var["F"] += decade_data_dictionary[each_year]['F']
                    else:
                        temp_dict_var["F"] = decade_data_dictionary[each_year]['F']
            else:
                dict_id = str(start) + ' - ' + str(end)
                decade_participation_dictionary[dict_id] = temp_dict_var
                start = end
                end = start + 10
                temp_dict_var = {}
       
        
        return decade_participation_dictionary


def show_plot(decade_patricipation_dictonary):
    male_patricipation = []
    female_patricipation = []
    decade_count = len(decade_patricipation_dictonary.keys())
    index_no = np.arange(decade_count)
    figx, ax = plt.subplots()
    
    for each_decade in decade_patricipation_dictonary:
        if 'M' in decade_patricipation_dictonary[each_decade]:
            male_patricipation.append(decade_patricipation_dictonary[each_decade]['M'])
        else:
            male_patricipation.append(0)
        if 'F' in decade_patricipation_dictonary[each_decade]:
            female_patricipation.append(decade_patricipation_dictonary[each_decade]['F'])
        else:
            female_patricipation.append(0)

    position = 0.2        
    plt.bar(index_no - position, male_patricipation, color='blue', label='Male', width=0.4)
    plt.bar(index_no + position, female_patricipation, color='red', label='Female', width=0.4)
    plt.xticks(index_no, decade_patricipation_dictonary.keys(), rotation=45)
    plt.xlabel('Decades')
    plt.ylabel('Number of Athlethes')
    plt.legend()
    plt.show()

decade_participation_dictionary = decade_participation("./athlete_events.csv")

show_plot(decade_participation_dictionary)



