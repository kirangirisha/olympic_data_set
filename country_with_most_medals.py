# Top 10 countries who have won most medals after 2000 - stacked column - split gold/silver/bronze
import csv
import operator
from collections import Counter 
import matplotlib.pyplot as plt
import numpy as np


def hosted_olympic_country(csv_path):
    
    with open(csv_path, 'r') as csv_file:
        matches_reader = csv.DictReader(csv_file)
        medals_dictionary = {}
        medal_tally = {}
        for row in matches_reader:    
            if(row['Year']) >= '2000':
                if(row['Team']) in medal_tally:
                    if row['Medal'] == "NA":
                        
                        continue
                    else:
                        if row['Medal'] in medal_tally[row['Team']]:
                            medal_tally[row['Team']][row['Medal']]+=1
                        else:
                            medal_tally[row['Team']][row['Medal']] =1
                        medals_dictionary[row['Team']] += 1    
                    
                else:
                    medals_dictionary[row['Team']] = 0

                    if row['Medal'] == "NA":
                        continue
                    else:
                        medals_dictionary[row['Team']] += 1
                        medal_tally[row['Team']] = {row['Medal']:1}
      
        temp = Counter(medals_dictionary) 

        country_with_most_medals = temp.most_common(10)
        top_ten_contries = {}
        
        for key in country_with_most_medals:
            top_ten_contries[key[0]] = medal_tally[key[0]]
        
        return top_ten_contries
      


def plot_top_10_countries(top_10_countries_data):
    medal_count = len(top_10_countries_data.keys())
    gold_medal = []
    silver_medal = []
    bronze_medal = []
    index_no = np.arange(medal_count)
    for each in top_10_countries_data:
        gold_medal.append(top_10_countries_data[each]['Gold'])
        silver_medal.append(top_10_countries_data[each]['Silver'])
        bronze_medal.append(top_10_countries_data[each]['Bronze'])
    plt.bar(index_no, gold_medal, label = 'gold', color = 'yellow')
    plt.bar(index_no, silver_medal, label = 'silver', bottom = gold_medal, color = 'grey')
    plt.bar(index_no, bronze_medal, label = 'bronze', bottom = list(map(lambda x,y:x+y,gold_medal,silver_medal)), color = 'brown')
    plt.xticks(index_no, top_10_countries_data.keys(), rotation = 45)
    plt.xlabel('Countries')
    plt.ylabel('Number of medals')
    plt.legend()
    plt.title('Top countries with most medals')
    plt.show()


top_10_countries_data = hosted_olympic_country("./athlete_events.csv")
plot_top_10_countries(top_10_countries_data)