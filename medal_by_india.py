# Find out all medal winners from India per season - Table
import csv

def indian_medal(csv_path):
    
    with open(csv_path, 'r') as csv_file:
        matches_reader = csv.DictReader(csv_file)
        medals_by_india_dictionary = {}

        for each in matches_reader:
            if each['Team'] == 'India' and each['Medal']  != "NA":
                if each['Year'] not in medals_by_india_dictionary:
                    medals_by_india_dictionary[each['Year']] = []
                    medals_by_india_dictionary[each['Year']].append(each['Name'])
                else:    
                    medals_by_india_dictionary[each['Year']].append(each['Name'])

                    
        return medals_by_india_dictionary
        
        
        

indian_medal("./india_specific_athlete_events.csv")




                