import unittest
import country_with_most_medals


class TestHostCity(unittest.TestCase):
    
    def test_hosting_city(self):
        actual_result = country_with_most_medals.hosted_olympic_country("./athlete_events.csv")

        expected_result = {'United States': {'Silver': 501, 'Bronze': 386, 'Gold': 674}, 
                           'Russia': {'Silver': 262, 'Bronze': 347, 'Gold': 304}, 
                           'Germany': {'Silver': 226, 'Gold': 260, 'Bronze': 289}, 
                           'Australia': {'Bronze': 247, 'Gold': 184, 'Silver': 262}, 
                           'China': {'Bronze': 164, 'Silver': 176, 'Gold': 253}, 
                           'Canada': {'Bronze': 167, 'Gold': 223, 'Silver': 118}, 
                           'Great Britain': {'Gold': 187, 'Bronze': 130, 'Silver': 156}, 
                           'France': {'Gold': 130, 'Silver': 164, 'Bronze': 134}, 
                           'Italy': {'Bronze': 171, 'Gold': 94, 'Silver': 143}, 
                           'Netherlands': {'Bronze': 102, 'Silver': 164, 'Gold': 123}
                           }

        self.assertDictEqual(actual_result, expected_result)
   

if __name__ == '__main__':
    unittest.main()
